<?//скрипт чистит заголовки товаров от мусорных фраз.
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Чистка h1");
if (CModule::IncludeModule("iblock")):
	CModule::IncludeModule("catalog");
	function nameUpdate($id,$name,$badWords){
		$newName = str_replace($badWords, "", $name);
		echo $id." - ".$newName."<br/>";
		$elem = new CIBlockElement;
		$arLoadProductArray = Array(
		  "NAME"    => $newName,
		);
		$res = $elem->Update($id, $arLoadProductArray);
	}
	$rs = CIBlockElement::GetList (
	        false,
	        Array("IBLOCK_ID" => 19,"ACTIVE" => "Y"),
	        false,
	        false,
	        Array('ID', 'NAME')
	    );
	$arBadName = array();
	while ($arElem = $rs->GetNext()){
		if(preg_match("/Москва/", $arElem["NAME"])){
			$arBadName[] = array($arElem["ID"], $arElem["NAME"]);
		}

		//nameUpdate($arElem["ID"],$arElem["NAME"]);
	}
	foreach ($arBadName as $badElem) {
			nameUpdate($badElem[0],$badElem[1]," Москва");
	}
endif;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>